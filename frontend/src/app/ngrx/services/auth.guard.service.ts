import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import * as fromRoot from '../reducers/index';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private store: Store<fromRoot.State>) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.store.pipe(
      select(fromRoot.getIsSessionRestored),
      filter(res => res !== undefined),
      switchMap(res => {
        if (!res) {
          this.router.navigate(['login']);
          return of(false);
        } else {
          return this.store.pipe(
            select(fromRoot.getIsLoggedIn),
            map(isLoggedIn => {
              if (!isLoggedIn) {
                this.router.navigate(['login']);
              }

              return isLoggedIn;
            })
          );
        }
      })
    );
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import {
  mapUserApiDataToUserData,
  User,
  UserApiData,
  UserProfilesApiData,
  UserStatus
} from '../../shared/models/user.data';

@Injectable()
export class TimeTrackerService {
  private baseUrl = 'http://192.168.88.120:3001/';

  public getHeaders() {
    return new HttpHeaders().set('Authorization', localStorage.getItem('jwt'));
  }
  // private baseUrl = 'http://localhost:3000/';
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<{ token: string }> {
    const body = { userId: username, password: password };
    return this.http.post<{ token: string }>(this.baseUrl + 'login', body);
  }

  checkSession(jwt: string): Observable<{ token: string }> {
    return this.http.get<{ token: string }>(
      this.baseUrl + 'login/session/' + jwt
    );
  }

  public getMonthData(
    year: number,
    month: number,
    userId: number
  ): Observable<{ CheckTime: string; CheckType: number }[]> {
    return this.http.get<{ CheckTime: string; CheckType: number }[]>(
      this.baseUrl +
        'data/month/' +
        month +
        '/year/' +
        year +
        '/users/' +
        userId,
      { headers: this.getHeaders() }
    );
  }

  public getUsers(): Observable<User[]> {
    return combineLatest([
      this.http
        .get<UserProfilesApiData[]>(this.baseUrl + 'data/users/profiles', {
          headers: this.getHeaders()
        })
        .pipe(distinctUntilChanged()),
      this.http
        .get<UserApiData[]>(this.baseUrl + 'data/users/online', {
          headers: this.getHeaders()
        })
        .pipe(distinctUntilChanged())
    ]).pipe(
      map(([profiles, usersOnline]: [UserProfilesApiData[], UserApiData[]]) => {
        const _online = mapUserApiDataToUserData(usersOnline);

        return profiles.map(p => {
          const _user = _online.find(o => o.id === +p.userid);
          if (_user) {
            return _user;
          } else {
            return {
              id: +p.userid,
              name: p.Name,
              status: UserStatus.Absent
            };
          }
        });
      })
    );
  }
}

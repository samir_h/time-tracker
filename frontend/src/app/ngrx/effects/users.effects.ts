import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { User } from '../../shared/models/user.data';
import { GET_USERS, setUsers } from '../actions/users.actions';
import { TimeTrackerService } from '../services/time-tracker.service';

@Injectable()
export class UsersEffects {
  @Effect()
  getUsers$ = this.actions$.pipe(
    ofType(GET_USERS),
    switchMap(() => {
      return this.timeTracker.getUsers().pipe(
        map((res: User[]) => {
          return setUsers({ users: res });
        })
      );
    })
  );
  constructor(
    private actions$: Actions,
    private timeTracker: TimeTrackerService
  ) {}
}

import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { mapToDailyData } from '../../shared/helpers';
import {
  CheckInEntryInterface,
  mapToCheckInEntryInterface
} from '../../shared/models/check-in-entry.interface';
import { getMonthData, setMonthData } from '../actions/month.actions';
import { TimeTrackerService } from '../services/time-tracker.service';
import * as monthActions from '../actions/month.actions';

@Injectable()
export class MonthEffects {
  @Effect()
  getMonthData$ = this.actions$.pipe(
    ofType(monthActions.GET_MONTH_DATA),
    switchMap(({ month, year, userId}) => {
      // NOTE: In future maybe here we can filter data for previous months, for now we will just take the actual month
      return this.monthService
        .getMonthData(year, month, userId)
        .pipe(
          map(res =>
            res.map((r: { CheckTime: string; CheckType: number }) =>
              mapToCheckInEntryInterface(r)
            )
          )
        )
        .pipe(
          map((data: CheckInEntryInterface[]) => {
            return setMonthData({ payload: { data: mapToDailyData(data) } });
          })
        );
    })
  );

  constructor(
    private actions$: Actions,
    private monthService: TimeTrackerService
  ) {}
}

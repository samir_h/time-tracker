import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, mapTo, switchMap, tap } from 'rxjs/operators';
import {
  LOGIN,
  LOGIN_SUCCESS,
  loginFail,
  loginSuccess,
  LOGOUT,
  RESTORE_SESSION,
  restoreSessionFail,
  restoreSessionSuccess
} from '../actions/auth.actions';
import { TimeTrackerService } from '../services/time-tracker.service';
import * as fromRoot from '../reducers/index';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType(LOGIN),
    switchMap(({ userId, password }) => {
      return this.timeTrackerService.login(userId, password).pipe(
        map((res: { token: string }) => {
          return loginSuccess({ jwt: res.token, userId: userId });
        }),
        catchError((error: HttpErrorResponse) => {
          this.store.dispatch(
            loginFail({ error: error.message || 'Something went wrong!' })
          );

          return of();
        })
      );
    })
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(LOGIN_SUCCESS),
    tap(({ jwt, userId }) => {
      localStorage.setItem('jwt', jwt);
      localStorage.setItem('imbusUserId', userId);
      this.router.navigate(['']);
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(LOGOUT),
    tap(() => {
      localStorage.removeItem('jwt');
      localStorage.removeItem('imbusUserId');
      this.router.navigate(['/login']);
    })
  );

  @Effect()
  restoreSession$ = this.actions$.pipe(
    ofType(RESTORE_SESSION),
    switchMap(() => {
      if (localStorage.getItem('jwt')) {
        return this.timeTrackerService
          .checkSession(localStorage.getItem('jwt'))
          .pipe(
            map((res: { token: string }) => {
              return restoreSessionSuccess({
                jwt: res.token
              });
            }),
            catchError(err => {
              return of(restoreSessionFail({ error: 'Unauthorized' }));
            })
          );
      } else {
        return of(restoreSessionFail({ error: 'No jwt in local storage' }));
      }
    })
  );

  constructor(
    private actions$: Actions,
    private timeTrackerService: TimeTrackerService,
    private router: Router,
    private store: Store<fromRoot.State>
  ) {}
}

import { createAction, props } from '@ngrx/store';
import { User } from '../../shared/models/user.data';

export const GET_USERS = 'GET_USERS';
export const SET_USERS = 'SET_USERS';

export const getUsers = createAction(GET_USERS);
export const setUsers = createAction(SET_USERS, props<{ users: User[] }>());

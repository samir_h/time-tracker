import { createAction, props } from '@ngrx/store';
import {
  CheckInEntryInterface,
  MonthData
} from '../../shared/models/check-in-entry.interface';
export const GET_MONTH_DATA = '[Month] Get Data';
export const SET_MONTH_DATA = '[Month] Set Data';

export const getMonthData = createAction(
  GET_MONTH_DATA,
  props<{ month: number; year: number; userId: number }>()
);

export const setMonthData = createAction(
  SET_MONTH_DATA,
  props<{ payload: { data: MonthData[] } }>()
);

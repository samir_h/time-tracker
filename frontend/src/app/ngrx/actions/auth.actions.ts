import { createAction, props } from '@ngrx/store';

export const LOGIN = '[Auth] Login';
export const LOGIN_SUCCESS = '[Auth] Login Success';
export const LOGIN_FAIL = '[Auth] Login Fail';
export const LOGOUT = '[Auth] Logout';

export const RESTORE_SESSION = '[Auth] Restore Session';
export const RESTORE_SESSION_SUCCESS = '[Auth] Restore Session Success';
export const RESTORE_SESSION_FAIL = '[Auth] Restore Session Fail';

export const login = createAction(
  LOGIN,
  props<{ userId: string; password: string }>()
);

export const loginSuccess = createAction(
  LOGIN_SUCCESS,
  props<{ jwt: string; userId: number }>()
);

export const loginFail = createAction(LOGIN_FAIL, props<{ error: string }>());

export const logout = createAction(LOGOUT);

export const restoreSession = createAction(RESTORE_SESSION);

export const restoreSessionSuccess = createAction(
  RESTORE_SESSION_SUCCESS,
  props<{ jwt: string }>()
);

export const restoreSessionFail = createAction(
  RESTORE_SESSION_FAIL,
  props<{ error: string }>()
);

import { Action, createReducer, on } from '@ngrx/store';
import {
  MonthData
} from '../../shared/models/check-in-entry.interface';
import { setMonthData } from '../actions/month.actions';

export interface State {
  data: MonthData[];
}

export const initialState: State = {
  data: []
};

const monthReducer = createReducer<State>(
  initialState,
  on(setMonthData, (state, { payload }) => {
    return { ...state, data: payload.data };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return monthReducer(state, action);
}

import { Action, createReducer, on } from '@ngrx/store';
import { User, UserStatus } from '../../shared/models/user.data';
import { setUsers } from '../actions/users.actions';

export interface State {
  users: User[];
}

export const initialState: State = {
  users: []
};

const monthReducer = createReducer<State>(
  initialState,
  on(setUsers, (state, { users }) => {
    return { ...state, users: users };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return monthReducer(state, action);
}

import { ActionReducerMap, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { MonthData } from '../../shared/models/check-in-entry.interface';
import { User, UserStatus } from '../../shared/models/user.data';
import * as auth from './auth.reducer.js';
import * as month from './month.reducer';
import * as users from './users.reducer';

export interface State {
  auth: auth.State;
  month: month.State;
  users: users.State;
}

export const reducers: ActionReducerMap<State> = {
  auth: auth.reducer,
  month: month.reducer,
  users: users.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];

// Month
const getMonthState = (state: State) => state.month;
export const getMonthData = createSelector(
  getMonthState,
  (state: month.State): MonthData[] => {
    return state.data;
  }
);

// Users
const getUsersState = (state: State) => state.users;
export const getUsersData = createSelector(
  getUsersState,
  (state: users.State) => state.users
);
export const getUsersPresentCount = createSelector(
  getUsersData,
  (_users: User[]) => {
    return _users.filter(u => u.status !== UserStatus.Absent).length;
  }
);

export const getUsersOnBreakCount = createSelector(
  getUsersData,
  (_users: User[]) => {
    return _users.filter(u => u.status === UserStatus.OnBreak).length;
  }
);

export const getUsersAbsentCount = createSelector(
  getUsersData,
  (_users: User[]) => {
    return _users.filter(u => u.status === UserStatus.Absent).length;
  }
);

// Auth
const getAuthState = (state: State) => state.auth;
export const getIsLoggedIn = createSelector(
  getAuthState,
  (state: auth.State) => state.loggedIn
);
export const getIsSessionRestored = createSelector(
  getAuthState,
  (state: auth.State) => state.sessionRestoreComplete
);

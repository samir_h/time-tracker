import { Action, createReducer, on } from '@ngrx/store';
import {
  loginFail,
  loginSuccess,
  logout,
  restoreSession,
  restoreSessionFail,
  restoreSessionSuccess
} from '../actions/auth.actions';

export interface State {
  loggedIn: boolean;
  jwt: string | undefined;
  error: string | undefined;
  sessionRestoreComplete: boolean | undefined;
  loading: boolean;
  // userId: string | undefined
}
export const initialState: State = {
  loggedIn: false,
  jwt: undefined,
  error: undefined,
  sessionRestoreComplete: undefined,
  loading: false
  // userId: undefined
};

const authReducer = createReducer<State>(
  initialState,
  on(loginSuccess, (state, { jwt }) => {
    return { ...state, loggedIn: true, jwt: jwt, sessionRestoreComplete: true };
  }),
  on(loginFail, (state, { error }) => {
    return { ...state, loggedIn: false, error: error };
  }),
  on(logout, state => {
    return { ...state, loggedIn: false, jwt: undefined };
  }),
  on(restoreSession, state => {
    return { ...state, loading: true };
  }),
  on(restoreSessionSuccess, (state, { jwt }) => {
    return {
      ...state,
      loggedIn: true,
      jwt: jwt,
      loading: false,
      sessionRestoreComplete: true
    };
  }),
  on(restoreSessionFail, (state, { error }) => {
    return {
      ...state,
      loggedIn: false,
      error: error,
      jwt: undefined,
      sessionRestoreComplete: false
    };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}

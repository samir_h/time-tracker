import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../app/ngrx/reducers/index';
import { restoreSession } from './ngrx/actions/auth.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private store: Store<fromRoot.State>) {
    this.store.dispatch(restoreSession());
  }
}

import { Component, Input } from '@angular/core';
import { User, UserStatus } from '../../shared/models/user.data';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent {
  public UserStatus = UserStatus;
  @Input() user: User;
  constructor() {}
}

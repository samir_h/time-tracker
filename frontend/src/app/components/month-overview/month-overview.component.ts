import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { MonthData } from '../../shared/models/check-in-entry.interface';
import * as fromRoot from '../../ngrx/reducers/index';

@Component({
  selector: 'app-month-overview',
  templateUrl: './month-overview.component.html',
  styleUrls: ['./month-overview.component.scss']
})
export class MonthOverviewComponent {
  /*  public displayedColumns = [
    'date',
    'in',
    'out',
    'break',
    'work-time',
    'total'
  ];*/

  public dataSource: MonthData[] = [];

  constructor(private store: Store<fromRoot.State>) {
    this.store.pipe(select(fromRoot.getMonthData)).subscribe(data => {
      this.dataSource = data;
    });
  }
}

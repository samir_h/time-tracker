import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { login } from '../../ngrx/actions/auth.actions';
import * as fromRoot from '../../ngrx/reducers/index';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public form = this.fb.group({
    username: [''],
    password: ['']
  });
  constructor(private fb: FormBuilder, private store: Store<fromRoot.State>) {}

  ngOnInit() {}

  public onSubmit() {
    this.store.dispatch(
      login({
        userId: this.form.value.username,
        password: this.form.value.password
      })
    );
  }
}

import { Routes } from '@angular/router';
import { MonthOverviewComponent } from '../../components/month-overview/month-overview.component';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { AuthGuard } from '../../ngrx/services/auth.guard.service';

export const AdminLayoutRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'monthly-overview',
    component: MonthOverviewComponent,
    canActivate: [AuthGuard]
  }
];

import { CheckTypeEnum } from './check-type.enum';

export interface CheckInEntryInterface {
  checkTime: Date;
  checkType: CheckTypeEnum;
}

export interface MonthData {
  date: string;
  in: Date | undefined;
  out: Date | undefined;
  breaks: Date[];
  workTime: Date | undefined;
  total: Date | undefined;
  breakTime: Date | undefined;
}

export function mapToCheckInEntryInterface(d: {
  CheckTime: string;
  CheckType: number;
}): CheckInEntryInterface {
  return {
    checkType: d.CheckType,
    checkTime: new Date(d.CheckTime)
  };
}

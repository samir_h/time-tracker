import { CheckTypeEnum } from './check-type.enum';

export interface User {
  id: number;
  name: string;
  status: UserStatus | undefined;
}

export enum UserStatus {
  Present,
  OnBreak,
  Absent
}

export interface UserApiData {
  Name: string;
  userid: string;
  CheckTime: string | undefined;
  CheckType: number | undefined;
}

export interface UserProfilesApiData {
  userid: string;
  Name: string;
}

export function mapUserApiDataToUserData(users: UserApiData[]): User[] {
  const newUsers: User[] = [];

  let _temp: {
    id: number;
    name: string;
    in: boolean;
    out: boolean;
    breaks: number;
  }[] = [];

  users.forEach(u => {
    if (!_temp.map(usr => usr.id).includes(+u.userid)) {
      _temp.push({
        id: +u.userid,
        name: u.Name,
        in: u.CheckType === CheckTypeEnum.In,
        out: false,
        breaks: 0
      });
    } else {
      _temp = [..._temp].map(usr => {
        if (+u.userid === usr.id) {
          if (u.CheckType === CheckTypeEnum.Out) {
            return { ...usr, out: true };
          } else if (u.CheckType === CheckTypeEnum.Break) {
            return { ...usr, breaks: usr.breaks + 1 };
          }
        }

        return usr;
      });
    }
  });

  _temp.forEach(t => {
    newUsers.push({
      id: t.id,
      name: t.name,
      status: calculateUserStatus(t.in, t.out, t.breaks)
    });
  });

  return newUsers;
}

export function calculateUserStatus(
  inTime: boolean,
  outTime: boolean,
  breaks: number
) {
  if (!inTime) {
    return UserStatus.Absent;
  } else if (inTime && outTime) {
    return UserStatus.Absent;
  } else if (inTime && !outTime && breaks % 2 === 1) {
    return UserStatus.OnBreak;
  } else if (inTime && !outTime && breaks % 2 === 0) {
    return UserStatus.Present;
  } else if (inTime && !outTime) {
    return UserStatus.Present;
  } else {
    return UserStatus.Absent;
  }
}

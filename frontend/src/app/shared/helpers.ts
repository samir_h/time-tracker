import {
  CheckInEntryInterface,
  MonthData
} from './models/check-in-entry.interface';
import { CheckTypeEnum } from './models/check-type.enum';
import { DatePipe } from './pipes/date.pipe';
import { TimePipe } from './pipes/time.pipe';

export function mapToDailyData(data: CheckInEntryInterface[]): MonthData[] {
  const datePipe = new DatePipe();

  let days: MonthData[] = [];

  data.forEach(d => {
    if (d.checkType === CheckTypeEnum.In) {
      days.push({
        date: datePipe.transform(d.checkTime),
        in: d.checkTime,
        out: undefined,
        breaks: [],
        breakTime: undefined,
        workTime: undefined,
        total: undefined
      });
    } else {
      days = days.map(day => {
        if (datePipe.transform(d.checkTime) === day.date) {
          if (d.checkType === CheckTypeEnum.Out) {
            return { ...day, out: d.checkTime };
          } else {
            return {
              ...day,
              breaks: [...day.breaks, d.checkTime].sort(
                (a, b) => (a.getTime() - b.getTime()) * -1
              )
            };
          }
        } else {
          return day;
        }
      });
    }
  });

  return days
    .map(d => {
      return {
        ...d,
        workTime: calculateWorkTime(d.in, d.out),
        breakTime: calculateBreakTime(d.breaks)
      };
    })
    .map(d => {
      return { ...d, total: calculateTotalHours(d.workTime, d.breakTime) };
    });
}

export function calculateBreakTime(breaks: Date[]): Date {
  const t = new Date(1970, 0, 1);
  if (breaks.length > 1) {
    let time = 0;
    let i = 0;
    while (i < breaks.length) {
      if (isOddNumber(breaks.length)) {
        i = 1;
      }

      time = time + (breaks[i].getTime() - breaks[i + 1].getTime());
      i = i + 2;
    }
    t.setTime(time - 3600000);
  } else {
    t.setTime(0 - 3600000);
  }

  return t;
}

export function isOddNumber(number: number): boolean {
  return number % 2 === 1;
}

export function calculateWorkTime(timeIn: Date, timeOut: Date): Date {
  const t = new Date(1970, 0, 1);
  if (timeOut) {
    t.setTime(timeOut.getTime() - timeIn.getTime() - 3600000);
  } else {
    t.setTime(new Date().getTime() - timeIn.getTime() - 3600000);
  }

  return t;
}

export function calculateTotalHours(workHours: Date, breakTime: Date) {
  const t = new Date(1970, 0, 1);
  t.setTime(workHours.getTime() - breakTime.getTime() - 3600000);

  return t;
}

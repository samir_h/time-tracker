import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date',
  pure: true
})
export class DatePipe implements PipeTransform {
  transform(value: Date): string {
    return value.toLocaleDateString('it-IT', {
      year: '2-digit',
      month: '2-digit',
      day: '2-digit',
    });
  }
}

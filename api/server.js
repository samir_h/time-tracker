//express server
var express = require('express'),
  app = express(),
  port = 3000;
var bodyParser = require('body-parser');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var isAuthenticated = require('./auth-middleware');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(cors());

//  key for JWT
var secretKey = 'imbusPeja';

// Get the adodb module
var ADODB = require('node-adodb');
ADODB.debug = true;

// Connect to the MS Access DB
var connection = ADODB.open(
  'DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=CrossChex.mdb;'
);

app.post('/login', function(req, res) {
  connection
    .query('SELECT * FROM V_UserClient;')
    .then(users => {
      var userId = req.body.userId;
      var password = req.body.password;

      if (!userId || !password) {
        return res
          .status(400)
          .send({ message: 'Wrong request data!', status: 400 });
      } else {
        if (users.some(u => u.Userid === userId && u.Pwd === password)) {
          var token = jwt.sign(req.body, secretKey, {
            expiresIn: 60 * 60
          });

          return res.status(200).send({ token: token });
        } else {
          return res
            .status(400)
            .send({ message: 'Username or Password invalid!', status: 400 });
        }
      }
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});

app.get('/login/session/:jwt', (req, res) => {
  if (req.params.jwt) {
    jwt.verify(req.params.jwt, secretKey, function(err, decoded) {
      if (err) {
        res.status(401).send('Unauthorized: Invalid token');
      } else {
        res.send({ token: req.params.jwt });
      }
    });
  } else {
    return res.status(401).send({ message: 'Unauthorized: Invalid token' });
  }
});

app.get('/data/month/:month/year/:year/users/:userId', isAuthenticated, (req, res) => {
  const year = req.params.year;
  const month = req.params.month;
  const userId = req.params.userId;

  if (!year || !userId || !month) {
    return res.status(500).send(err);
  }

  connection
    .query(
      'SELECT CheckTime, CheckType FROM Checkinout where userid like ' +
        userId +
        ' and Year(CheckTime) = ' +
        year +
        ' and Month(CheckTime) = ' +
        month +
        ' order by CheckTime asc;'
    )
    .then(users => {
      return res.send(users);
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});

app.get('/data/users/online', isAuthenticated, (req, res) => {
  const date = new Date();
  const query =
    'SELECT V_UserClient.Name,V_UserClient.userid, Checkinout.CheckTime, Checkinout.CheckType from V_UserClient left join Checkinout on (V_UserClient.userid = Checkinout.userid) where Year(CheckTime) = ' +
    date.getFullYear() +
    ' and Month(CheckTime) = ' +
    (date.getMonth() + 1) +
    ' and Day(CheckTime) = ' +
    date.getDate() +
    ' order by V_UserClient.userid, Checkinout.CheckType asc';
  connection
    .query(query)
    .then(users => {
      return res.json(users);
    })
    .catch(err => {
      return res.status(500).send(err);
    });
});

app.get('/data/users/profiles',isAuthenticated, (req, res) => {
  const query = 'SELECT userid, Name from V_UserClient';
  connection
    .query(query)
    .then(users => {
      res.send(users);
    })
    .catch(err => {
      return res.status(400).send(err);
    });
});

app.listen(port);

console.log('REST API server started on port: ' + port);

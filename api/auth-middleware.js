const jwt = require('jsonwebtoken');
const secret = 'imbusPeja';

const isAuthenticated = function(req, res, next) {
  const token = req.headers['authorization'];

  if (!token) {
    res.status(401).send('Unauthorized: No token provided');
  } else {
    jwt.verify(token, secret, function(err, decoded) {
      if (err) {
        res.status(401).send('Unauthorized: Invalid token');
      } else {
        next();
      }
    });
  }
};

module.exports = isAuthenticated;
